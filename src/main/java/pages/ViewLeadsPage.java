package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods{

	public ViewLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="viewLead_firstName_sp")
	private WebElement eleFname;
	public ViewLeadsPage verifyFirstName(String expTxt) {
		verifyExactText(eleFname,expTxt);
		return new ViewLeadsPage();
	}
		
	@FindBy(xpath="(//a[@class='subMenuButton'])[3]")
	private WebElement eleEditBtn;
	public EditLeadsPage clickEditBtn() {
		click(eleEditBtn);
		return new EditLeadsPage();
	}
	
	@FindBy(id="viewLead_companyName_sp")
	private WebElement eleCname;
	public ViewLeadsPage verifyCompName(String expTxt) {
		verifyPartialText(eleCname,expTxt);
		return new ViewLeadsPage();
	}
	
	@FindBy(className="subMenuButtonDangerous")
	private WebElement eleDelBtn;
	public LeadsPage clickDelBtn() {
		click(eleDelBtn);
		return new LeadsPage();
	}
	
	@FindBy(linkText="Duplicate Lead")
	private WebElement eleDupBtn;
	public LeadsPage clickDupBtn() {
		click(eleDupBtn);
		return new LeadsPage();
	}
	
	
}
