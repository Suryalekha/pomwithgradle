package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LeadsPage extends ProjectMethods{

	public LeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead")
	private WebElement eleCreateLeadLink;
	public CreateLeadPage createLead() {
		//WebElement eleCreateLeadLink = locateElement("LinkText","Leads");
		click(eleCreateLeadLink);
		return new CreateLeadPage();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement eleFindLeadsLink;
	public FindLeadsPage findLead() {
		click(eleFindLeadsLink);
		return new FindLeadsPage();
	}
	
	@FindBy(linkText="Merge Leads")
	private WebElement eleMergeLead;
	public MergeLeadsPage clickMergeLead() {
		click(eleMergeLead);
		return new MergeLeadsPage();
	}
	
	@FindBy(id="sectionHeaderTitle_leads")
	private WebElement eleDupTitle;
	public LeadsPage verifyDupTitle(String dupTitle) {
		verifyExactText(eleDupTitle,dupTitle);
		return new LeadsPage();
	}
	
	@FindBy(className="smallSubmit")
	private WebElement eleSubmit;
	public ViewLeadsPage clickSubmit() {
		click(eleSubmit);
		return new ViewLeadsPage();
	}
}
