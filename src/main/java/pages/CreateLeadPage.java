package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_companyName")
	private WebElement eleCompName;
	public CreateLeadPage typeCname(String cname) {
		type(eleCompName,cname);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_firstName")
	private WebElement eleFirstName;
	public CreateLeadPage typeFirstname(String fname) {
		type(eleFirstName,fname);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_lastName")
	private WebElement eleLastName;
	public CreateLeadPage typeLastname(String lname) {
		type(eleLastName,lname);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_dataSourceId")
	private WebElement eleSource;
	public CreateLeadPage selectSource(String source) {
		selectDropDownUsingText(eleSource,source);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement eleMarkCamp;
	public CreateLeadPage selectMarketCamp(String marCamp) {
		selectDropDownUsingText(eleMarkCamp,marCamp);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_industryEnumId")
	private WebElement eleInds;
	public CreateLeadPage selectIndustry(String ind) {
		selectDropDownUsingText(eleInds,ind);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_ownershipEnumId")
	private WebElement eleOwner;
	public CreateLeadPage selectOwnership(String owner) {
		selectDropDownUsingText(eleOwner,owner);
		return new CreateLeadPage();
	}
	
	@FindBy(id="createLeadForm_currencyUomId")
	private WebElement eleCurr;
	public CreateLeadPage selectCurrency(String currency) {
		selectDropDownUsingText(eleCurr,currency);
		return new CreateLeadPage();
	}
	
	@FindBy(className="smallSubmit")
	private WebElement eleSubmit;
	public ViewLeadsPage clickSubmit() {
		click(eleSubmit);
		return new ViewLeadsPage();
	}
}
