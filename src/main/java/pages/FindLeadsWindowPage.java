package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsWindowPage extends ProjectMethods{

	public FindLeadsWindowPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="firstName")
	private WebElement eleFirstName;
	public FindLeadsWindowPage typeName(String fname) {
		type(eleFirstName,fname);
		return new FindLeadsWindowPage();
	}
	
	@FindBy(xpath="(//button[@class='x-btn-text'])[1]")
	private WebElement eleFindLBtn;
	public FindLeadsWindowPage clickFLBtn() {
		click(eleFindLBtn);
		return new FindLeadsWindowPage();
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[1]")
	private WebElement eleFlId;
	public FindLeadsWindowPage getFLeadID() {
		mleadID = getText(eleFlId);
		return new FindLeadsWindowPage();
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[1]")
	private WebElement eleFLid;
	public MergeLeadsPage selFirstLeadID() {
		clickWithNoSnap(eleFLid);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[6]")
	private WebElement eleSLid;
	public MergeLeadsPage selSecondLeadID() {
		clickWithNoSnap(eleSLid);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
}
