package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods {

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText="Leads")
	private WebElement eleLeadsLink;
	public LeadsPage Leads() {
		//WebElement eleLeadsLink = locateElement("LinkText","Leads");
		click(eleLeadsLink);
		return new LeadsPage();
	}

}





