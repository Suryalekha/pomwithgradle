package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//img[@src='/images/fieldlookup.gif'])[1]")
	private WebElement eleFromIcon;
	public FindLeadsWindowPage selFromLead() {
		click(eleFromIcon);
		switchToWindow(1);
		return new FindLeadsWindowPage();
	}
	
	@FindBy(xpath="(//img[@src='/images/fieldlookup.gif'])[2]")
	private WebElement eleToIcon;
	public FindLeadsWindowPage selToLead() {
		click(eleToIcon);
		switchToWindow(1);
		return new FindLeadsWindowPage();
	}
	
	@FindBy(linkText="Merge")
	private WebElement eleMerge;
	public LeadsPage clickMergeBtn() {
		clickWithNoSnap(eleMerge);
		acceptAlert();
		return new LeadsPage();
	}
}
