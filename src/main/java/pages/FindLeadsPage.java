package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//input[@name='firstName'])[3]")
	private WebElement eleFName;
	public FindLeadsPage typeFName(String fname) {
		type(eleFName,fname);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="(//input[@style='width: 212px;'])[1]")
	private WebElement eleFLeadID;
	public FindLeadsPage typeMerLeadID() {
		type(eleFLeadID,mleadID);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement eleFindLeadsBtn;
	public FindLeadsPage clickFindLeadBtn() {
		click(eleFindLeadsBtn);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[4]")
	private WebElement eleFirstIDLink;
	public ViewLeadsPage clickFirstID() {
		click(eleFirstIDLink);
		return new ViewLeadsPage();
	}
	
	@FindBy(className="x-paging-info")
	private WebElement eleErr;
	public FindLeadsPage verifyMessage(String expTxt) {
		verifyPartialText(eleErr,expTxt);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="//span[text()='Phone']")
	private WebElement elePhone;
	public FindLeadsPage clickPhone() {
		click(elePhone);
		return new FindLeadsPage();
	}
	
	@FindBy(name="phoneAreaCode")
	private WebElement elePhAreaCode;
	public FindLeadsPage typePhCode(String code) {
		type(elePhAreaCode,code);
		return new FindLeadsPage();
	}
	
	@FindBy(name="phoneNumber")
	private WebElement elePhNum;
	public FindLeadsPage typePhNum(String num) {
		type(elePhNum,num);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[4]")
	private WebElement eleLeadID;
	public FindLeadsPage getFirstLeadID() {
		delLeadID = getText(eleLeadID);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="(//input[@style='width: 212px;'])[1]")
	private WebElement eleDelFLeadID;
	public FindLeadsPage typeDelLeadID() {
		type(eleDelFLeadID,delLeadID);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="//span[text()='Email']")
	private WebElement eleEmail;
	public FindLeadsPage clickEmail() {
		click(eleEmail);
		return new FindLeadsPage();
	}
	
	@FindBy(name="emailAddress")
	private WebElement eleEmailAddr;
	public FindLeadsPage typeEmail(String addr) {
		type(eleEmailAddr,addr);
		return new FindLeadsPage();
	}
	
	@FindBy(xpath="(//a[@class='linktext'])[4]")
	private WebElement eleDupLeadID;
	public FindLeadsPage getFirstDupLeadID() {
		dupLeadID = getText(eleDupLeadID);
		return new FindLeadsPage();
	}
	
}
