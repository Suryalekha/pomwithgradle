package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadsPage extends ProjectMethods{

	public EditLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="(//input[@name='companyName'])[2]")
	private WebElement eleCmpName;
	public EditLeadsPage typeCompName(String ccname) {
		type(eleCmpName,ccname);
		return new EditLeadsPage();
	}
	
	
	@FindBy(xpath="(//input[@class='smallSubmit'])[1]")
	private WebElement eleUpdatBtn;
	public ViewLeadsPage clickUpdate() {
		click(eleUpdatBtn);
		return new ViewLeadsPage();
	}
}
