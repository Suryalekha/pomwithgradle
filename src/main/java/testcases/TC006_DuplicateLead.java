package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class TC006_DuplicateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC006_DuplicateLead";
		testDescription="Duplicate lead in leaftaps";
		authors="Suryalekha";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC006";
	}


	@Test(dataProvider="fetchData")
	public void duplicateLead(String emaddr,String dupTitle,String fname) {
		new HomePage()
		
		.clickCRMSFA()
		.Leads()
		.findLead()
		.clickEmail()
		.typeEmail(emaddr)
		.clickFindLeadBtn()
		.getFirstDupLeadID()
		.clickFirstID()
		.clickDupBtn()
		.verifyDupTitle(dupTitle)
		.clickSubmit()
		.verifyFirstName(fname);
	}

}
