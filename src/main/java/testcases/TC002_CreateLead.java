package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.HomePage;
import pages.LeadsPage;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="Create lead in leaftaps";
		authors="Suryalekha";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC002";
	}


	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd,String cname, String fname,String lname,String source,String marCamp,String ind,String owner,String currency) {
		new HomePage()
		
		.clickCRMSFA()
		.Leads()
		.createLead()
		.typeCname(cname)
		.typeFirstname(fname)
		.typeLastname(lname)
		.selectSource(source)
		.selectMarketCamp(marCamp)
		.selectIndustry(ind)
		.selectOwnership(owner)
		.selectCurrency(currency)
		.clickSubmit()
		.verifyFirstName(fname);
	}


}
