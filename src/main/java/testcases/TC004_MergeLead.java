package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class TC004_MergeLead  extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC004_MergeLead";
		testDescription="Merge lead in leaftaps";
		authors="Suryalekha";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC004";
	}


	@Test(dataProvider="fetchData")
	public void mergeLead(String fname,String errMsg) {
		new HomePage()
		
		.clickCRMSFA()
		.Leads()
		.clickMergeLead()
		.selFromLead()
		.typeName(fname)
		.clickFLBtn()
		.getFLeadID()
		.selFirstLeadID()
		.selToLead()
		.typeName(fname)
		.clickFLBtn()
		.selSecondLeadID()
		.clickMergeBtn()
		.findLead()
		.typeMerLeadID()
		.clickFindLeadBtn()
		.verifyMessage(errMsg);
	}

}
