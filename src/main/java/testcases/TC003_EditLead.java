package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testDescription="Edit lead in leaftaps";
		authors="Suryalekha";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC003";
	}


	@Test(dataProvider="fetchData")
	public void editLead(String fname,String ccname) {
		new HomePage()
		
		.clickCRMSFA()
		.Leads()
		.findLead()
		.typeFName(fname)
		.clickFindLeadBtn()
		.clickFirstID()
		.clickEditBtn()
		.typeCompName(ccname)
		.clickUpdate()
		.verifyCompName(ccname);
	}


}
