package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC005_DeleteLead";
		testDescription="Delete lead in leaftaps";
		authors="Suryalekha";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC005";
	}


	@Test(dataProvider="fetchData")
	public void delLead(String phcode,String num,String expTxt) {
		new HomePage()
		
		.clickCRMSFA()
		.Leads()
		.findLead()
		.clickPhone()
		.typePhCode(phcode)
		.typePhNum(num)
		.clickFindLeadBtn()
		.getFirstLeadID()
		.clickFirstID()
		.clickDelBtn()
		.findLead()
		.typeDelLeadID()
		.clickFindLeadBtn()
		.verifyMessage(expTxt);
	}

}
